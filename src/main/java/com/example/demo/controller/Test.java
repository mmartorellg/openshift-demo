package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/hello")
@RestController
public class Test {

    private final Logger log = LoggerFactory.getLogger(Test.class);

    public Test() {
    }

    @GetMapping("/{name}")
    public ResponseEntity<String> deleteCountry(@PathVariable String name) {
        log.debug("REST request to show hello name : {}", name);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body("!!!!!!Hello " + name + "!!!");
    }
}
